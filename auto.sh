#!/bin/bash
# Script permettant d'automatiser l'utilisation de live build

# Il faudra éventuellement penser à isoler les fonctions dans un fichier à part
# puis les importer de la manière suivante :

# . function_file.sh


main() {

    # Fonction principale qui sera exécuté à la dernière ligne du fichier.
    # Cela permet de placer le cœur du script avant la définition des fonctions.

    echo -e "Script de préparation Live Build\n"
    def_arch


    # On délare un booléen pour pouvoir réafficher le menu select à chaque tour
    # de boucle (break se trouvant dans chaque entrée du case).

    keep_in=true
    while [ $keep_in = true ]
    do
        echo
        PS3="
Action à exécuter : "
        options=("lb config" "Nommer l'iso" "Définir un live.list.chroot" \
            "Ajouter des .deb" "Remplir /etc/skel" "Remplir /root" \
            "Définir un hook.chrook" "lb build" "Tout ce qui précède" \
            "lb clean" "Vider config" "Redéfinir l'architecture" "Quitter")
        select opt in "${options[@]}"
        do
            case $opt in
                "lb config")
                    echo
                    lb config
                    break
                    ;;
                "Nommer l'iso")
                    echo
                    name_iso
                    break
                    ;;
                "Définir un live.list.chroot")
                    echo
                    package_list
                    break
                    ;;
                "Ajouter des .deb")
                    echo
                    packages_sup
                    break
                    ;;
                "Remplir /etc/skel")
                    echo
                    includes_skel
                    break
                    ;;
                "Remplir /root")
                    echo
                    includes_root
                    break
                    ;;
                "Définir un hook.chrook")
                    echo
                    hook_chroot
                    break
                    ;;
                "lb build")
                    echo
                    sudo lb build
                    keep_in=false
                    break
                    ;;
                "Tout ce qui précède")
                    echo
                    lb config
                    name_iso
                    package_list
                    packages_sup
                    includes_skel
                    includes_root
                    hook_chroot
                    sudo lb build
                    keep_in=false
                    break
                    ;;
                "lb clean")
                    echo
                    clean_up
                    break
                    ;;
                "Vider config")
                    echo
                    clean_config
                    break
                    ;;
                "Redéfinir l'architecture")
                    echo
                    def_arch
                    break
                    ;;
                "Quitter")
                    keep_in=false
                    break
                    ;;
                *)
                    echo -e "\nErreur lors de la saisie"
                    break
                    ;;
            esac
        done
    done
}


# Definition des fonctions

def_arch() {
    # Fonction définissant l'architecture
    read -p "Architecture : 32 ou 64 ? " arch
    while [ $arch != '32' ] && [ $arch != '64' ]
    do
        echo "Erreur lors de la saisie"
        read -p "Architecture : 32 ou 64 bit ? " arch
    done
    cp -r auto/config_$arch  auto/config
    sed -i s/utilisateur/$USER/g auto/config
    sed -i s/langue/$LANG/g auto/config
}

name_iso() {
    # Fonction définissant le nom de l'iso
    read -p "Nom de l'iso ? " nom
    sed -i s/^Name:.*/Name:\ $nom/g config/build
}

package_list() {
    # Fonction définissant la liste des paquets supplémentaires
    cp auto/live.list.chroot config/package-lists/live.list.chroot
}

packages_sup() {
    # Fonction permettant d'ajouter des paquets .deb supplémentaires
    cp auto/packages/*.deb config/packages.chroot/
}

includes_skel() {
    # Fonction remplissant /etc/skel
    for dir in `ls /home/$USER`
    do
        mkdir -p config/includes.chroot/etc/skel/$dir
    done
    # la boucle suivante permet de créer les dossiers nécessaires avant de
    # copier les fichiers adéquats à l'intérieur.
    # L'astuce est dans l'expression régulière ${var%/*} qui va découper le
    # dernier élément de chemin. Le grep / permet de filtrer les fichiers pour
    # lesquels il est nécessaire de procéder à cet manipulation.
    # Je n'ai trouver plus simple : ni cp --parents ni install -D ne se
    # comportaient de la manière que je voulais...
    for dir_t in `grep / auto/includes.etc.skel.txt`
    do
        mkdir -p config/includes.chroot/etc/skel/${dir_t%/*}
    done
    for file in `cat auto/includes.etc.skel.txt`
    do
        cp -pR /home/$USER/$file config/includes.chroot/etc/skel/$file
    done
    cp  auto/post-install.sh config/includes.chroot/etc/skel/
}

includes_root() {
    # Fonction remplissant la racine
    for file in `cat auto/includes.root.txt`
    do
        mkdir -p config/includes.chroot${file%/*}
        sudo cp -pR $file config/includes.chroot$file
    done
}

hook_chroot() {
    # Fonction définissant un hook pour la phase de chroot
    cp auto/1000-perso.hook.chroot config/hooks/
}

clean_up() {
    # Fonction permettant de lancer lb clean avec ou sans option
    read -p "Lancer la commande avec ou sans option lb clean ? (O/option) " rep0
    while [ $rep0 != 'chroot' ] && [ $rep0 != 'source' ] \
        && [ $rep0 != 'purge' ] && [ $rep0 != 'binary' ] \
        && [ $rep0 != 'stage' ] && [ $rep0 != 'remove' ] \
        && [ $rep0 != 'cache' ] && [ $rep0 != 'O' ] && [ $rep0 != 'all' ]
    do
        echo "Erreur lors de la saisie"
        read -p \
            "Lancer la commande avec ou sans option lb clean ? (O/option) " rep0
    done
    if [ $rep0 = 'O' ]
    then
        sudo lb clean
    else
        sudo lb clean --$rep0
    fi
}

clean_config() {
    # Fonction vidant le répertoire config
    rm -rf config/
}


# Exécution du script
main "$@"
